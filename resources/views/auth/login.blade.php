@extends('layouts.frame') @section('title', 'Login') @section('content')

<section class="hero is-dark is-fullheight">
    <div class="hero-head">@include ('layouts.nav')</div>
    <div class="hero-body">
        <div class="column is-one-third is-offset-one-third">
            <form
                class="form-horizontal"
                role="form"
                method="POST"
                action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="login-form formed">
                    <p class="control has-icon has-icon-right">
                        <span class="icon user">
                            <i class="fa fa-user"></i>
                        </span><input
                            id="email"
                            type="text"
                            class="input email-input"
                            name="email"
                            placeholder="johnsmith@gmail.com"
                            value="{{ old('email') }}"
                            required="required"
                            autofocus="autofocus">

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </p>
                    <p class="control has-icon has-icon-right">
                        <span class="icon user">
                            <i class="fa fa-lock"></i>
                        </span><input
                            id="password"
                            type="password"
                            class="input password-input"
                            placeholder="password"
                            name="password"
                            required="required">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </p>
                    <br/>
                    <p class="control login">
                        <button
                            class="button is-success is-outlined is-large is-fullwidth"
                            type="submit">Login</button>
                    </p>
                    <div class="section forgot-password">
                            <p class="has-text-centered">
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot password</a>
                            </p>
                        </div>
                </div>

            </form>
        </div>
    </div>
</section>
@endsection
