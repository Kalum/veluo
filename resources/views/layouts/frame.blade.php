<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') | Veluo</title>
        @yield('css')
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/board.css') }}">
        <link rel="stylesheet" href="{{ asset('css/veluo.css') }}">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <script src="{{ asset('js/app.js') }}"></script>

        @yield('js')
    </head>
<body>
        @yield('content')
    </body>
</html>
<style>
.element {
    background-color: #3500d3;
background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1600 900'%3E%3Cpolygon fill='%2347299f' points='957 450 539 900 1396 900'/%3E%3Cpolygon fill='%2318005e' points='957 450 872.9 900 1396 900'/%3E%3Cpolygon fill='%23221078' points='-60 900 398 662 816 900'/%3E%3Cpolygon fill='%232c165c' points='337 900 398 662 816 900'/%3E%3Cpolygon fill='%23342d7a' points='1203 546 1552 900 876 900'/%3E%3Cpolygon fill='%232a1948' points='1203 546 1552 900 1162 900'/%3E%3Cpolygon fill='%230d0b58' points='641 695 886 900 367 900'/%3E%3Cpolygon fill='%23433854' points='587 900 641 695 886 900'/%3E%3Cpolygon fill='%23090744' points='1710 900 1401 632 1096 900'/%3E%3Cpolygon fill='%232b2433' points='1710 900 1401 632 1365 900'/%3E%3Cpolygon fill='%23392f57' points='1210 900 971 687 725 900'/%3E%3Cpolygon fill='%23282828' points='943 900 1210 900 971 687'/%3E%3C/svg%3E");
background-attachment: fixed;
background-size: cover;
background-position: center center;
background-repeat: no-repeat;
background-size: cover;
}

    .formed {
  font-size: 1.1em;
  color: #bdbdbd;
  position: relative;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);
  overflow: hidden;
    }
</style>

