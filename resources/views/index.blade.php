@extends('layouts.frame')
@section('title', 'Home')
@section('content')
<section class="hero is-dark is-fullheight element">
    <div class="hero-head">@include ('layouts.nav')</div>

    <div class="container has-text-centered">
        <div class="notification is-success">
            <button class="delete"></button>
            Kalum's Testing Web Server
        </div>

        <div class="logo">
            <div class="line topLine"></div>
            <div class="content">
                <div class="title is-1 has-text-primary name">LAMP server attempt #1</div>

                <div class="desc">I hope this works</div>
                <div class="since">
                        <i class="fa fa-copyright"></i>
                        2019</div>
            </div>

            <div class="line bottomLine"></div>
        </div>
    </div>
</div>
</section>@endsection
<script src="https://code.jquery.com/jquery-3.4.1.min.js"integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="crossorigin="anonymous"></script>
<style>

.container {
  margin-top: 30px;
}


.notification > button.delete {
  border: none;
}

.is-hidden {
  display: none;
}
@import url(https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400);
body {
  background: #212121;
}

@keyframes openTopLine {
  0% {
    width: 0;
    transform: translateY(5em);
  }
  50% {
    width: 27em;
    transform: translateY(5em);
  }
  100% {
    width: 27em;
    transform: translateY(0);
  }
}
@keyframes openBottomLine {
  0% {
    width: 0;
    transform: translateY(-5em);
  }
  50% {
    width: 27em;
    transform: translateY(-5em);
  }
  100% {
    width: 27em;
    transform: translateY(0);
  }
}
@keyframes slideContent {
  0%, 30% {
    transform: translateY(50%);
    opacity: 0;
  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
}
.logo {
  font-size: 1.1em;
  text-align: center;
  color: #bdbdbd;
  position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);
  overflow: hidden;
}
.logo .line {
  height: 1px;
  background: #bdbdbd;
  display: inline-block;
  width: 27em;
}
.logo .line.topLine {
  animation: openTopLine 1.5s;
}
.logo .line.bottomLine {
  animation: openBottomLine 1.5s;
}
.logo .content {
  animation: slideContent 1s;
  animation-fill-mode: backwards;
  animation-delay: 1.25s;
}
.logo .content .vintage {
  font-weight: 400;
  font-size: 2em;
  text-transform: uppercase;
  text-shadow: 1px 1px 0px #212121, 2px 2px 0px #bdbdbd;
  letter-spacing: 4px;
}
.logo .content .since {
  font-weight: 100;
  margin: 0.25em auto 0.5em;
}
.logo .content .desc {
  font-weight: 300;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-style: italic;
}
.logo .content .skills {
  font-size: 1.2em;
  margin-top: 0.25em;
  font-weight: 400;
  letter-spacing: 1px;
}
.logo .content .links a {
  display: inline-block;
  color: #bdbdbd;
  font-size: 1.25em;
  margin: 0.25em 0.2em 0;
  transition: color 0.1s;
  cursor: pointer;
  transition: color 0.15s;
  animation: slideContent 1s;
  animation-fill-mode: backwards;
}
.logo .content .links a:nth-of-type(1n) {
  animation-delay: 2s;
}
.logo .content .links a:nth-of-type(2n) {
  animation-delay: 2.25s;
}
.logo .content .links a:nth-of-type(3n) {
  animation-delay: 2.5s;
}
.logo .content .links a:nth-of-type(4n) {
  animation-delay: 2.75s;
}
.logo .content .links a:nth-of-type(5n) {
  animation-delay: 3s;
}
.logo .content .links a:hover {
  color: white;
}

</style>
<script>
$(document).on('click', '.notification > button.delete', function() {
    $(this).parent().addClass('is-hidden');
    return false;
});

       </script>
